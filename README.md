**Run a JMeter script (.jmx file) in a Docker container**

In its current form, the Docker container must execute the script from the present working directory.
Raw results will be saved in "$(pwd)/results/${TEST_NAME}_Xu_Yi.csv" where "X" is the number of users and "Y" the number of iterations to execute.
An HTML dashboard will be created in $(pwd)/results/${TEST_NAME}/dashboard/index.html
---

## Building the Docker container

We may want to host container image(s) to allow a pull rather than a build, but until then build it by executing:

docker build -t perftest_jmeter:latest .

*note: the user.properties file enables creation of the results csv and dashboard.
As such, it is required for the container to build properly (copied from $(pwd) to the container)

We will probably want another container image without this functionality in order to publish results to a pesistence tier rather than the local dashboard in the future.
---

## Executing a test

Running the JMeter test is driven by the jmeterDocker.sh (bash) script, which takes the following parameters:

	-i <Number of iterations to execute>
    -n <Name of the JMeter test script (without the ".jmx" extension)>
    -p <Name of the properties file setting variables defined in the JMeter file>
    -t <Fully qualified path of $(pwd).  As in the folder containing the artifacts (Dockerfile, JMeter script, etc...)>
	-u <Number of users to execute>
    -? displays the above help text, 'cause, that's kinda helpful

Notes:

-t: For some reason Docker doesn't like relative paths or path variables. I've only been able to make this work with the fully qualified path
-p: JMeter script variables are defined in a jmx file with "${__property(PROP_NAME)}".  They are set via the command line using -JPROP_NAME=value or read from a properties file set by -q.  In this case "threads" and "iterations" are provided via the command line in jmeterDocker.sh while others are read from the properties file.

So, for example, if you were to clone this repo in to /Users/yourname/Testing/JMeter/docker/ you would execute a single user, single iteration test of the example "CreateWorkspaceAPICollectionThenDelete_NoListener.jmx" JMeter script like this:

sh jmeterDocker.sh -n CreateWorkspaceAPICollectionThenDelete_NoListener -p test.properties -t /Users/yourname/Testing/JMeter/docker/ -i 1 -u 1

---

## Caveat(s) about the example jxm script (CreateWorkspaceAPICollectionThenDelete_NoListener.jmx)

With the example test.properties file the test will run against Stage environment servers:

This test logs in, creates a new (personal) workspace, creates an api, adds schema to the new api, creates a collection for the new api, then deletes the collection, the api then the workspace and logs the user out.

Logons are for usernames in the form of "Thread-N" (where "N" is the number of the thread currently being executed) with the password "PerfTesting".  These users are defined in a team I created in the Stage environment and belong to team domain "performance-testing.postman-stage.co".  There are only 10 users currently created and part of the team, so executing more than 10 users (e.g: setting -u > 10) will results in errors.
