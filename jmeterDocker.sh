#!/bin/bash

function prepareResultsDir()
{
    #remove any pre-existing results and dashboard files
    #or create results directories if they don't exist
    if [ -d "$(pwd)/results" ];
    then
        rm $(pwd)/results/*.csv
        if [ -d "$(pwd)/results/${TEST_NAME}/dashboard" ];
        then
            rm -rf $(pwd)/results/${TEST_NAME}/*
        else
            mkdir -p $(pwd)/results/${TEST_NAME}/dashboard
        fi
    else
        mkdir -p $(pwd)/results/${TEST_NAME}/dashboard
    fi
}

function executeDockerizedJMeter()
{
    prepareResultsDir

    export volume_path=$(pwd) && \
    export jmeter_path=/mnt/jmeter && \
    docker run \
    -v "${volume_path}:${jmeter_path}" \
    perftest_jmeter \
    -n \
    -q ${jmeter_path}/$PROPERTIES_FILE \
    -Jthreads=${USERS} \
    -Jiterations=${ITERATIONS} \
    -t ${jmeter_path}/${TEST_NAME}.jmx \
    -l ${jmeter_path}/results/${TEST_NAME}_${USERS}u_${ITERATIONS}i.csv \
    -e \
    -o ${jmeter_path}/results/${TEST_NAME}/dashboard
}

function displayUsage()
{
	echo
	echo JMeter commandline wrapper script.
	echo 
	echo Valid Options:
	echo
	echo "?		Displays Help"
 	echo "-i <N>      Number of iterations to execute"
    echo "-n <JMeter Script Name (without .jmx extension)>" 
	echo "-p <path>   Properties file for test variables passed in to JMeter scripts"
	echo "-t <path>   Test Folder path containing scripts and properties files. Results will be saved here under /results"
	echo "-u <N>      Virtual users to execute"
	echo 
}

while getopts "i:j:n:p:t:u:" OPTIONS;
do
	case $OPTIONS in
	    i) ITERATIONS=$OPTARG;;
        n) TEST_NAME=$OPTARG;;
        p) PROPERTIES_FILE=$OPTARG;;
        t) TEST_DIR=$OPTARG;;
	    u) USERS=$OPTARG;;
	    \?) displayUsage ;;
	esac
done

executeDockerizedJMeter