#!/bin/bash

set -e
freeMem=`awk '/MemFree/ { print int($2/1024) }' /proc/meminfo`
s=$(($freeMem/10*8))
x=$(($freeMem/10*8))
n=$(($freeMem/10*4))
export JVM_ARGS="-Xmn${n}m -Xms${s}m -Xmx${x}m"

echo "JMeter JVM ARGS=${JVM_ARGS}"
echo "JMeter CMD ARGS=$@"

# Keep entrypoint simple: we must pass the standard JMeter arguments
jmeter $@